/* pkglist.h
 *
 * a collection of subroutines that deal specifically with
 * managing Package*'s within List*'s
*/

#ifndef __PKGLIST_H
#define __PKGLIST_H

#include "common.h"
#include "list.h"
#include "pkgread.h"

/* note: the trailing / is important esp. if it's a symlink such as on
 * modern systems.
 * FIXME this whole thing needs to be smarter and configurable. */

#define BASEDIR /var/log/packages/

/* compare a List node to a Package*. Mainly used as a comparison
 * function to be plugged into list_search (see list.h) */

int list_search_package(void* item, void* void_pkg);

/* return a List* of *strings* containing package database entry
 * filenames. You will need to append these to the BASEDIR under some
 * circumstances. */

List* get_all_packages(void);

/* free all Package*'s and all non-NULL elements of same that are used
 * as -> item nodes in a List*. Not to be confused with destroy_package,
 * which is declared in pkgread.h, which this function calls. */

int destroy_all_packages(List* pkglist);

#endif
