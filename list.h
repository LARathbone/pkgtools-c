#ifndef __LIST_H
#define __LIST_H

#include "common.h"

/* TYPE DECLARATIONS */

/* generic linked list. */

typedef struct _list List;
struct _list {
	void* item;
	List* next;
};

/* FUNCTION DECLARATIONS */

/* initialize a List. Must be the first function run. Takes no
 * arguments; returns a pointer to the stringlist created. */

List* list_init(void);

/* pre-sort sanity check to make sure we're not dealing with a List with
 * only a head. */

int list_sort_check(List* lp);

/* provide it a List head, and it will walk along a List chain and
 * free all members and all Lists, including the one provided. */

int list_destroy(List* lp);

/* add a string as an initial entry 'head' to list. This must be run
 * before using _add_string. Returns TRUE if success and FALSE if
 * failure. */

int list_add_head_str(List* lp, const char* str);

/* generic version of the above. */
int list_add_head(List* lp, void* node);

/* convenience function to add a string to a List which already has a
 * head. Memory allocation of the new string is baked into this function.
 * */

List* list_add_string(List* lp, const char* str);

/* generic version of the above. */
/* add a node of any pointer type to a List which already has a head. You
 * must allocate memory for the node, but not for the new List */

List* list_add_node(List* lp, void* node);

/* swap items held between 2 List* 's */

int list_item_swap(List* a, List* b);

/* provide it the head of a List of strings, and it will sort it
 * alphabetically (bubble sort). Returns true upon success, false upon
 * failure. */

int list_sort_str_abc(List* lp);

/* FIXME - new stuff being worked on */

/* generic function to search a List for a node. It needs to be
 * provided with a pointer to the List you are searching, a pointer to
 * the target ->item candidate you are searching for (void*) and a
 * function pointer to a function that will compare a node to a target
 * and return an int (TRUE if match, FALSE otherwise. list.c does
 * provide a convenience function, below, to search a node against a
 * target string. */

List* list_search(List* lp, void* targ, int (*sfunc)(void*, void*));

/* convenience function that can easily be passed as a pointer to
 * list_search, if the target you happen to want to compare against the
 * -> item nodes in question is a string. */

int list_search_str(void*, void*);

/* deletes a node by providing its List* address. You need to free the
 * deleted node yourself afterwards. */

int list_delete_node(List* lp, List* node);

#endif
