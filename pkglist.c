#include "pkglist.h"

/* subroutines*/

int list_search_package(void* item, void* void_pkg) {
        Package* srch_pkg;
        Package* poss_pkg;
	int memb_cnt = 0;
	int memb_max = 5;	/* ->name, desc, ver, arch, rel */

        srch_pkg = (Package*)void_pkg;
        poss_pkg = (Package*)item;

        /* sanity check */

	/* I'm hoping this also gets around the issue of "old school"
	 * name-only packages slipping through the cracks. Ie, if we vet
	 * out 'name'-less packages at the sanity check stage, we can
	 * just do a strcmp against all characteristics and we don't have
	 * to worry about an all-null package matching with another
	 * all-null package. */

	if (!srch_pkg || !poss_pkg ||
			!srch_pkg->name || !poss_pkg->name) {

                if (debug) printf("list_search_package: "
                                "pointers provided are not "
                                "Package*'s. Aborting.\n");
                return FALSE;
        }

        /* --- */

	/* note - we already tested above that ->name's != NULL. */

        if ((strcmp(srch_pkg->name, poss_pkg->name) == 0)) {
		++memb_cnt;		/* 1 */
	}

	if ( (srch_pkg->desc) && (poss_pkg->desc) ) {
		if (strcmp(srch_pkg->desc, poss_pkg->desc) == 0) {
			++memb_cnt;	/* 2 */
		}
	}

	if ( (srch_pkg->ver) && (poss_pkg->ver) ) {
		if (strcmp(srch_pkg->ver, srch_pkg->ver) == 0) {
			++memb_cnt;	/* 3 */
		}
	}

	if ( (srch_pkg->arch) && (poss_pkg->arch) ) {
		if (strcmp(srch_pkg->arch, srch_pkg->arch) == 0) {
			++memb_cnt;	/* 4 */
		}
	}

	if ( (srch_pkg->rel) && (poss_pkg->rel) ) {
		if (strcmp(srch_pkg->rel, srch_pkg->rel) == 0) {
			++memb_cnt;	/* 5 */
		}
	}

	if ( (!srch_pkg->desc) && (!poss_pkg->desc) &&
		(!srch_pkg->ver) && (!poss_pkg->ver) &&
		(!srch_pkg->arch) && (!poss_pkg->arch) &&
		(!srch_pkg->rel) && (!poss_pkg->rel) ) {

		if (debug) printf("list_search_package: "
			"node and target both have valid names but "
			"both have no other members defined. "
			"Assuming it's an old school package. "
			"Registering a match:  %s ; %s\n",
			srch_pkg->name, poss_pkg->name);

		memb_cnt = memb_max;
	}

	/* the final test... */

	if (memb_cnt == memb_max) {
                /* match */
                if (debug) printf("list_search_package: match.\n");
                return TRUE;
        } else {
        	/* if all else fails... */
	        return FALSE;
	}
}

List* get_all_packages(void) {
	/* get an initial string-List */
	List* slp = list_init();

	/* basedir FIXME - HARDCODED */
	const char* basedir = QW(BASEDIR);

	/* obtain a pointer to a DIR* stream - fed to readdir */
	DIR* dp = opendir(basedir);

	/* define a pointer to a 'dirent' struct - returned by readdir */
	struct dirent* dir;

	/* define a stat struct - needs to be fed to lstat to get file
	 * status and be read from to test file status */
	struct stat sb;

	/* --- */

	/* loop through basedir and add regular files to linked list. */
	
	while ( (dir = readdir(dp)) != NULL ) {
		char buf[BUFLEN] = "";

		/* build string to full path of pkg file */
		strcpy(buf, basedir);
		strncat(buf, dir->d_name, BUFLEN-strlen(basedir));

		/* run lstat on the file - abort if failure */
		assert ( lstat(buf, &sb) != -1 );

		/* see 'man lstat' */
		/* test if path fed to lstat is a regular file. If it's
		 * not, start the loop over again. */

		if ( (sb.st_mode & S_IFMT) != S_IFREG )
			continue;
		
		/* ------------------------------------------- */

		/* found regular file */

		if (slp->item == NULL) {
			if (debug) printf("Adding head: %s\n",
					dir->d_name);
			assert( list_add_head_str(slp, dir->d_name) );
		} else {
			if (list_add_string(slp, dir->d_name) == NULL) {
				fprintf(stderr, "Fatal error.\n");
				exit(1);
			}
		}
	}

	/* free the DIR we no longer need */
	closedir(dp);

	/* sort the list we've generated */
	list_sort_str_abc(slp);

	return slp;
}

int destroy_all_packages(List* pkglist) {
	List* trk;
	void* ptr;
	Package* poss_pkg;

	/* sanity check */
	if (! pkglist) {
		if (debug) fprintf(stderr, "destroy_all_packages: NULL "
				"List provided. Aborting.\n");
		return FALSE;
	}
	ptr = pkglist->item;
	poss_pkg = (Package*)ptr;

	if ( ! poss_pkg->name ) {
		if (debug) fprintf(stderr, "destroy_all_packages: This "
				"is not a List of Package*'s. "
				"Aborting.\n");
		return FALSE;
	}

	/* --- */
	if (debug) printf("Destroying all packages in List...\n");

	trk = pkglist;

	while (trk != NULL) {
		ptr = trk->item;
		poss_pkg = (Package*)ptr;
		if ( poss_pkg->name ) {
			assert(destroy_package(poss_pkg));
			/* important to set this to NULL so that
			 * list_destroy doesn't try to double-free it
			 * later. */
			trk->item = NULL;
		}
		if (trk->next == NULL) break;
		trk = trk->next;
	}

	return TRUE;
}
