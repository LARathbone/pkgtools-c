#ifndef __PKGREAD_H
#define __PKGREAD_H

#include "common.h"

/* TYPE DECLARATIONS */

typedef struct _package {
	char* name;
	char* desc;
	char* ver;
	char* arch;
	char* rel;
} Package;

/* PUBLIC FUNCTION DECLS */

/* this builds a *full* Package struct from a package file database
 * entry in /var/log/packages and returns a pointer to it. Memory
 * allocation is baked into the function. */

Package* init_package_from_file(const char* file);

Package* init_package(const char* name, const char* desc,
		const char* ver, const char* arch, const char* rel);

/* takes a "fullname", eg, foo-1.0-i386-2, and populates a Package
 * structure given a pointer to same. */

Package* init_package_from_fullname(const char* fullname);

int package_add_description(Package* pkg, const char* desc);

/* free the elements of the pkg, assuming not NULL  - returns TRUE if
 * success, FALSE if failure. Need a double pointer here so we can
 * actually set the underlying Package* pointer to null. */

int destroy_package(Package* pkg);

/* NEW STUFF WORKING ON */

/* take a package desc and strip out single newlines which are generally
 * just where the desc is wrapping at approx. 72 characters. We'll leave
 * in place *double* newlines, which are generally going to be a true
 * paragraph break in the description. Returns a pointer back to the
 * desc for convenience upon success; NULL otherwise. */

char* package_desc_strip_newlines(char* desc);

#endif
