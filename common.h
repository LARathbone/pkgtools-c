#ifndef __COMMON_H
#define __COMMON_H

#define _XOPEN_SOURCE 600

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef BUFLEN
#define BUFLEN 1024
#endif

#ifndef DEBUG
#define DEBUG 1
#endif

/* stringize macro. */
/* don't use _QW() in code; use QW() */
#define QW(s) _QW(s)
#define _QW(s) #s

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

/* global variables */

extern int debug;

#endif
