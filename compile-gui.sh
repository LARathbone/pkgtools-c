#!/bin/sh

CC=gcc
DEBUG=${DEBUG:-1}

$CC -o slacknaptic \
	slacknaptic.c pkgread.c list.c pkglist.c dir.c \
	-DDEBUG=${DEBUG} \
	-g \
	-Wall -Wextra -pedantic -std=c99 \
	`pkg-config --libs --cflags gtk+-3.0` \
	-rdynamic
