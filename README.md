# pkgtools-c

Experimental project to implement various Slackware pkgtools-related
subroutines in C.

Test 'main' function in dir.c shows some things it can do so far.

To compile, run ./compile.sh

There is also a GUI (codename: slacknaptic; haven't heard from marketing
yet on that one) that I have started, but I have shifted focus away from
that for now to continue work on the subroutines.

To switch the build to the GUI, for now you'll have to manually un-if0
the main function of slacknaptic.c and #if0-out the main function in
dir.c.  Then, run ./compile-gui.sh instead of ./compile.sh.
