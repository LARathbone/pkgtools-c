#include "pkgread.h"

/* --- GLOBAL VARIALES --- */

/* line counter for _fgets. */
static int ln = 0;

/* --- MACROS --- */

/* allocate memory for a package element and set up a string for it to
 * point to. PKG is a Package* where the element is to be allocated, and
 * ELEM is the element variable name, which MUST match with the members
 * of Package (ie: name, ver, desc, arch and rel). Make sure you set up
 * your ELEM variables as local arrays large enough to fit the string
 * you'd like to allocate. */

#define PACKAGE_ALLOC_ELEM(PKG, ELEM) \
	if (ELEM != NULL) { \
		if ( strlen(ELEM) > 0 && (strlen(ELEM)+1) <= BUFLEN ) { \
			PKG->ELEM = malloc(strlen(ELEM) + 1); \
			assert( strcpy(PKG->ELEM, ELEM) != NULL ); \
		} else if (strlen(ELEM) > 0 && (strlen(ELEM)+1) > BUFLEN) { \
			fprintf(stderr, "FATAL ERROR: Elements greater " \
				"than %d not supported. Aborting.\n", BUFLEN); \
				exit(1); \
		} else { \
			if (debug) printf("Empty string found for %s." \
				"Allocating no space.\n", #ELEM); \
			PKG->ELEM = NULL; \
		} \
	} else { \
		if (debug) printf("null pointer passed for %s." \
				"Allocating no space.\n", #ELEM); \
		PKG->ELEM = NULL; \
	}

/* frees a package ELEM (name, ver, desc, arch, rel) from a Package* PKG.
 * The rules with matching variables don't apply here, since we're only
 * working with Package* members and no associated variables. */

#define PACKAGE_FREE_ELEM(PKG, ELEM) \
	if ((PKG)->ELEM != NULL) { \
		free((PKG)->ELEM); \
	} 


/* PRIVATE FUNCTION DECLS */

/* _fgets
 * provide our own version of fgets which allows you to feed it an int*
 * as a pointer to a line counter. Running the function acts the same
 * as regular fgets, but it increments the value of the int* you feed it.
 * Returns TRUE if operation successful; FALSE otherwise. */

static int _fgets(char* s, int n, FILE* stream, int* ct);


/* PUBLIC FUNCTIONS */

Package* init_package(const char* name, const char* desc,
		const char* ver, const char* arch, const char* rel) {

	/* initialize pkg pointer that we'll build and return at end */	
	Package* pkg = malloc( sizeof(Package) );

	/* allocate memory for the strings of the package */

	PACKAGE_ALLOC_ELEM(pkg, name)
	PACKAGE_ALLOC_ELEM(pkg, desc)
	PACKAGE_ALLOC_ELEM(pkg, ver)
	PACKAGE_ALLOC_ELEM(pkg, arch)
	PACKAGE_ALLOC_ELEM(pkg, rel)

	return pkg;
}

Package* init_package_from_fullname(const char* fullname) {
	/* pointer for what we'll return */
	Package* pkg;

	/* generic counter */
	long unsigned int i;	/* damnit C! Stop yelling at me! */

	/* dashct - how many dashes have we encountered */
	int dashct = 0;

	/* local variables to hold package metadata */
	char name[BUFLEN] = "";
	char ver[BUFLEN] = "";
	char arch[BUFLEN] = "";
	char rel[BUFLEN] = "";

	/* temp local buffer to work with */
	char buf[BUFLEN] = "";

	/* sanity check */
	if (fullname == NULL) return NULL;

	/* dumb sanity check for fullname */
	if ( strlen(fullname) < 1 ) {
		if (debug) fprintf(stderr,
				"init_package_from_fullname: "
				"Dubious string provided.\n");
		return NULL;
	}

	/* count dashes */
	for (i = 0; i < strlen(fullname)-1; ++i) {
		if (fullname[i] == '-' ) {
			++dashct;
			if (debug >= 2) printf("dashct: %d\n", dashct);
		}
	}

	/* old-school slackware packages would just be 'name.t?z'
	 * any dashes are just part of the name. eg, name-devel.tgz
	 * any mixing and matching will probably fail and just be
	 * considered part of the pkg name, which is how pkgtools
	 * works anyway. */
	if (dashct < 3) {
		strncpy(name, fullname, BUFLEN-1);
		if (debug) printf("Old-school name.t?z detected: "
				"%s\n", name);

	} else {	/* if (dashct >= 3)  */
		/* looking at the code of installpkg, it expects 3 dashes for
		 * ver, arch and rel. Any further dashes are assumed to be part
		 * of the package name. */

		/* secondary dash counter */
		int j = 0;

		/* copy fullname to the buf to work with. */
		strncpy(buf, fullname, BUFLEN-1);

		/* work backwards, because anything left over is part
		 * of the package name. */
		for (i = strlen(fullname)-1; i > 0; --i) {
			if (fullname[i] == '-') {
				if (debug) printf("Dash detected at posn: "
						"%lu\n", i);

				/* break string up into pieces based on
				 * the "3 dash" rule. */

				/* rel */
				if (j == 0) {
					strcpy(rel, &buf[i+1]);
					buf[i] = '\0';
					++j;
					if (debug) printf("rel set to: "
							"%s\n", rel);

				/* arch */
				} else if (j == 1) {
					strcpy(arch, &buf[i+1]);
					buf[i] = '\0';
					++j;
					if (debug) printf("arch set to: "
							"%s\n", arch);

				/* ver */
				} else if (j == 2) {
					strcpy(ver, &buf[i+1]);
					buf[i] = '\0';
					++j;
					if (debug) printf("ver set to: "
							"%s\n", ver);

				}
			}	/* if fullname[i] = '-' */
		}	/* backwards for loop */

		/* take rest of spit it into 'name' */
		strcpy(name, buf);	/* could use strncpy, but would be
					   redundant */
	}	/* dashct >= 3 */

	pkg = init_package(name, NULL, ver, arch, rel);

	return pkg;
}

int package_add_description(Package* pkg, const char* desc) {
	/* sanity checks */
	if ( pkg == NULL || desc == NULL ) {
		if (debug) printf("package_add_description failed. "
				"Null pointer passed to function.\n");
		return FALSE;
	}

	if (pkg->desc != NULL) {
		if (debug) printf("package_add_description failed. "
				"Tried to add description but -> desc "
				"pointer is not NULL.\n");
		return FALSE;
	}

	/* if we get past the sanity checks, we can proceed. */
	PACKAGE_ALLOC_ELEM(pkg, desc)
	return TRUE;
}

int destroy_package(Package* pkg) {
	PACKAGE_FREE_ELEM(pkg, name)
	PACKAGE_FREE_ELEM(pkg, desc)
	PACKAGE_FREE_ELEM(pkg, ver)
	PACKAGE_FREE_ELEM(pkg, arch)
	PACKAGE_FREE_ELEM(pkg, rel)

	/* free the pkg, assuming not null */
	if (pkg != NULL) {
		free(pkg);
		return TRUE;
	} else {
		return FALSE;
	}
}

Package* init_package_from_file(const char* file) {
	/* in this function we also utilize the GLOBAL variable, `ln' */

	/* Package* we'll return at the end. */
	Package* pkg;

	/* Stream for the package file database entry. */
	FILE* fp;

	/* general buffer used throughout this subroutine. */
	char buf[BUFLEN];

	/* buffer to hold full package name including version, arch etc.
	 * /sbin/installpkg calls this 'shortname', ironically. */
	char pkgfullnam[BUFLEN];

	/* buffer to hold package description. Yes, this will always be
	 * enough space. */
	char desc[BUFLEN] = "";

	/* 'name with colon' */
	char nwc[BUFLEN];	/* I'm lazy, sue me. */

	/* 'name with colon (and) space' :/ */
	char nwcs[BUFLEN+1];	/* without the 1, gcc generates a warning
				   because it's not sure that by using the
				   same buflen for 'nwc' and 'nwc ' that it
				   won't overwrite by 1 character. */

	/* --- */

	if (debug) printf("init_package_from_file: opening %s\n", file);
	fp = fopen(file, "r");

	assert (fp != NULL);

	while ( _fgets(buf, BUFLEN, fp, &ln) ) {
		if (debug >= 2) printf("line no of FILE: %d\n", ln);
		switch (ln) {
			case 0:
				/* this _shouldn't_ happen, but if it does,
				 * catch it and break. */
				break;
			case 1:
				/* sanity check - there is no reason why
				 * this function should return anything
				 * but '1'.  If it does not, the first
				 * line was not in proper slackware
				 * format or serious error. */

				assert( (sscanf(buf, "PACKAGE NAME: %s",
							pkgfullnam)) == 1);
				if (debug >= 2) printf("pkgfullnam: %s\n",
						pkgfullnam);
				if (debug >= 2) printf("initting package"
						" from fullname.\n");
				assert(pkg = init_package_from_fullname(pkgfullnam));
				break;
			case 2:
				/* COMPRESSED PACKAGE SIZE: ...
				 * don't care. */
				break;
			case 3:
				/* UNCOMPRESSED PACKAGE SIZE: ...
				 * don't care. */
				break;
			case 4:
				/* PACKAGE LOCATION: ...
				 * don't care. */
				break;
			case 5:
				/* PACKAGE DESCRIPTION: (own line)
				 * don't care. */
				break;
			default:
				/* now that we've got past the lines we don't
				 * care about, look for lines that start with
				 * 'packagename:' which contain the description
				 */

				/* populate vars with the pkgname with a colon,
				 * and a colon + a space */
				assert( sprintf(nwc, "%s:", pkg->name) );
				assert( sprintf(nwcs, "%s ", nwc) );

				if ( strncmp(buf, nwc, strlen(nwc)) == 0 ) {
					if (debug >= 2) printf("strncmp"
						" match found: "
						"buf: %s\n", buf);

					/* if they're an exact match (or plus 1
					 * accounting for newline character),
					 * we know it's an empty line. */

					if (strcmp(buf, nwc) == 0 ||
						strcmp(buf, nwcs) == 0) {
							strcat(desc, "\n");
					} else {
						/* cat at most 72
						 * characters from
						 * buffer (after 'nwc')
						 * to the desc character
						 * array. Adding 1 to
						 * account for the space
						 * after colon */

						strncat(desc, &buf[strlen(nwc)+1], 72);
					}
				}

				/* FALLTHRU */

		}	/* switch */
	}	/* while */
	
	fclose(fp);

	/* reset line no to 0 */
	ln = 0;

	if (debug) printf("Package name: %s\n", pkg->name);
	if (debug) printf("Package version: %s\n", pkg->ver);
	if (debug) printf("Package arch: %s\n", pkg->arch);
	if (debug) printf("Package rel: %s\n", pkg->rel);

	/* FIXME - desc could be blank, and this assertion would fail. */
	if (debug >= 2) printf("pkgread: gonna add description: "
			"%s\n", desc);
	assert( package_add_description(pkg, desc) );

	if (debug) printf("Package desc: %s\n", pkg->desc);

	return pkg;
}

char* package_desc_strip_newlines(char* desc) {
	char tmp[BUFLEN];
	char* cp = NULL;
	int desc_ln = strlen(desc);

	/* sanity check */
	if (!desc || desc_ln < 1) {
		if (debug) printf("package_desc_strip_newlines: "
			"NULL description provided. Aborting.\n");
		return NULL;
	}

	/* somewhat arbitrary, but better to be safe than sorry */
	assert( desc_ln < BUFLEN-1 );

	/* --- */

	strncpy(tmp, desc, BUFLEN-1);

	/* HACK ALERT */
	/* dec 29 in ASCII table is "group separator". Sounds legit. */

	/* swap newlines for space - 2nd newline in a double newline is
	 * converted to a 'group separator' ascii character 29 */

	while ( (cp = strchr(tmp, '\n')) ) {
		if (cp[1] == '\n') {
			cp[0] = cp[1] = 29;
		} else {
			cp[0] = ' ';
		}
	}

	/* change group separator back to newline. */

	while ( (cp = strchr(tmp, 29)) ) {
		cp[0] = '\n';
	}

	if (debug) printf("stripped desc: %s\n", tmp);

	strncpy(desc, tmp, desc_ln);

	return desc;
}

/* PRIVATE FUNCTIONS */

static int _fgets(char* s, int n, FILE* stream, int* ct) {
	assert (ct != NULL);
	if ( fgets(s, n, stream) != NULL ) {
		if (debug >= 2) printf("_fgets: incrementing *ct.\n");
		++(*ct);
		return TRUE;
	} else {
		return FALSE;
	}
}
