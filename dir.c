#include "common.h"
#include "list.h"
#include "pkglist.h"
#include "pkgread.h"

/* global variables defined */

int debug = DEBUG;


/* MAIN - TEST FUNCTION ONLY */

int main(void) {
	List* file_list;
	List* pkg_list;
	List* trk;
	Package* pkg;
	int cnt;
	char fn[BUFLEN] = "";

	char* search_term = "xwd-1.0.7-x86_64-2";
	List* tmp;
	List* tmp2;
	Package* poss_pkg = NULL;

	/* build file_list linked list which will be used to create
	 * pkg_list */
	file_list = get_all_packages();
	assert (file_list != NULL);

	/* try out list_search */

	tmp = list_search(file_list, search_term, &list_search_str);
	if (tmp != NULL) {
		printf("match = %s\n", (char*)tmp->item);
	}

	/* --- */

	trk = file_list;

	/* build pkg_list */

	/* boolean to see if 'first' entry has been added. */
	/* FIXME - this is dumb -  I need to get rid of the head-specific
	 * functions and make things smarter. */
	cnt = 0;

	pkg_list = list_init();

	while (trk != NULL) {
		strcpy(fn, QW(BASEDIR));
		strcat(fn, (char*)trk->item);

		if (debug) printf("Adding to pkg_list: %s\n", fn);

		if (cnt == 0) { 
			if (debug) printf("cnt is 0. "
				"Initting pkg_list with head.\n");
			pkg = init_package_from_file(fn);
			assert( list_add_head(pkg_list, pkg) );
			cnt = 1;
		} else {
			if (debug) printf("cnt is not 0. Continuing pkg->list.\n");
			pkg = init_package_from_file(fn);
			assert( list_add_node(pkg_list, pkg) );
		}

		trk = trk->next;
	}

	/* try out list_search */
	/* 2nd argument is arbitrary. just pickinga rando node somewhere a few
	 * entries in. */

	tmp2 = pkg_list->next->next->next->next;
	tmp = list_search(pkg_list, tmp2->item, &list_search_package);

	if (tmp != NULL) {
		poss_pkg = (Package*)tmp->item;
		printf("pkg match = %s\n", (char*)poss_pkg->name);
	} else {
		printf("no match :(\n");
	}

	/* test out delete */

	assert( list_delete_node(pkg_list, tmp2) );
	assert( destroy_package(tmp2->item) );
	free(tmp2); 

	/* clean up */
	assert( list_destroy(file_list) );
	assert( destroy_all_packages(pkg_list) );
	assert( list_destroy(pkg_list) );

	return 0;
}
