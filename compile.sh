#!/bin/sh

DEBUG=${DEBUG:-1}

gcc pkgread.c list.c pkglist.c dir.c -o test.bin -DDEBUG=${DEBUG} -g -Wall -Wextra -Wimplicit-fallthrough=3 -pedantic -std=c89 -ansi
