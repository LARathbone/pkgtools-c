#include <gtk/gtk.h>

#include "common.h"
#include "list.h"
#include "pkglist.h"
#include "pkgread.h"

#define GLADE_FILE "slacknaptic.glade"
#define APP_ID "com.gitlab.LARathbone.slacknaptic"

#define BUILDER builder
#define GET_OBJECT(X) gtk_builder_get_object(BUILDER, #X)
#define GET_WIDGET(X) GTK_WIDGET(GET_OBJECT(X))

/* see .glade file COMMENTED xml */
enum _liststore_column_names {
	gchararray_name_col,
	gchararray_ver_col,
	gchararray_arch_col,
	gchararray_rel_col,
	gchararray_desc_col
};

/* global vars */

GtkBuilder* BUILDER;
int debug = DEBUG;


/* function declarations */

static void activate(GtkApplication* app);

static void populate_treeview(void);

void on_treeview_row_activated(GtkTreeView* treeview,
	GtkTreePath* path, GtkTreeViewColumn* col);


/* function definitions */

int main (int argc, char** argv) {
	GtkApplication* app;
	int status;

	app = gtk_application_new(APP_ID, G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);

	status = g_application_run (G_APPLICATION(app), argc, argv);

	g_object_unref(app);
	return status;
}

static void activate(GtkApplication* app) { /* , gpointer user_data) { */
	GtkWidget* appwindow;

	/* global defined */
	BUILDER = gtk_builder_new_from_file(GLADE_FILE);

	gtk_builder_connect_signals(BUILDER, BUILDER);

	appwindow = GET_WIDGET(appwindow);
	gtk_application_add_window(app, GTK_WINDOW(appwindow));
	gtk_widget_show_all(appwindow);

	populate_treeview();
}

static void populate_treeview(void) {
	/* widget pointers */
	GtkListStore*	liststore;

	/* other decls */
	GtkTreeIter	iter;
	List*		file_list = NULL;
	List*		pkg_list = NULL;
	List*		tracker = NULL;
	Package*	pkg = NULL;
	int		pl_has_head = FALSE;
	char		buf[BUFLEN];

	/* --- */

	/* build file_list linked list which will be used to create
	 * pkg_list */

	file_list = get_all_packages();
	assert(file_list);

	pkg_list = list_init();
	assert(pkg_list);

	tracker = file_list;

	while (tracker) {
		strcpy(buf, QW(BASEDIR));
		strncat(buf, (char*)tracker->item,
				BUFLEN - strlen(QW(BASEDIR)) - 1);

		if (!pl_has_head) {
			if (debug) printf("pl_has_head is 0. "
				"Initting pkg_list with head.\n");
			pkg = init_package_from_file(buf);
			assert( list_add_head(pkg_list, pkg) );

			pl_has_head = TRUE;

		} else {
			if (debug) printf("pl_has_head is not 0. "
				"Continuing pkg_list.\n");
			pkg = init_package_from_file(buf);
			assert( list_add_node(pkg_list, pkg) );
		}

		tracker = tracker->next;
	}

	if (debug) printf("file_list done\n");

	pkg = NULL;
	tracker = NULL;

	/* populate gui */

	liststore = GTK_LIST_STORE(GET_OBJECT(liststore));

	tracker = pkg_list;
	assert(tracker);

	/* fill up with name and ver from pkg_list */
	while (tracker) {
		pkg = (Package*)tracker->item;
		assert(pkg);

		if (pkg->desc) {
			assert( package_desc_strip_newlines(pkg->desc) );
			assert( g_strchomp(pkg->desc) );
		}

		gtk_list_store_append(liststore, &iter);

		gtk_list_store_set(liststore, &iter, gchararray_name_col,
				(char*)pkg->name,
				-1);

		gtk_list_store_set(liststore, &iter, gchararray_ver_col,
				(char*)pkg->ver,
				-1);

		gtk_list_store_set(liststore, &iter, gchararray_arch_col,
				(char*)pkg->arch,
				-1);

		gtk_list_store_set(liststore, &iter, gchararray_rel_col,
				(char*)pkg->rel,
				-1);

		gtk_list_store_set(liststore, &iter, gchararray_desc_col,
				(char*)pkg->desc,
				-1);

		tracker = tracker->next;
	}

	assert( list_destroy(file_list) );
	assert( destroy_all_packages(pkg_list) );
	assert( list_destroy(pkg_list) );
}

void on_treeview_row_activated(GtkTreeView* treeview,
	GtkTreePath* path, GtkTreeViewColumn* col) {

	GtkWindow* appwindow;
	GtkWidget* dialog;
	GtkTreeIter iter;
	GtkTreeModel* model;

	gchar* name = NULL;
	gchar* ver = NULL;
	gchar* arch = NULL;
	gchar* rel = NULL;
	gchar* desc = NULL;

	appwindow = GTK_WINDOW(GET_OBJECT(appwindow));
	model = gtk_tree_view_get_model(treeview);
	
	if (gtk_tree_model_get_iter(model, &iter, path)) {
		gtk_tree_model_get(model, &iter,
				gchararray_name_col, &name,
				gchararray_ver_col, &ver,
				gchararray_arch_col, &arch,
				gchararray_rel_col, &rel,
				gchararray_desc_col, &desc,
				-1);
	}

	dialog = gtk_message_dialog_new_with_markup(appwindow,
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_INFO,
				GTK_BUTTONS_CLOSE,
				"<b>%s-%s-%s-%s</b>\n\n%s",
				name, ver, arch, rel, desc);

	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	g_free(name);
	g_free(ver);
	g_free(arch);
	g_free(rel);
	g_free(desc);
}
