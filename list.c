#include "list.h"

/* subroutines */

List* list_init(void) {
	/* "list pointer" = lp */
	List* lp;

	lp = malloc( sizeof(List) );

	/* if malloc failed, abort. */
	if (lp == NULL) return NULL;

	lp->item = NULL;
	lp->next = NULL;

	return lp;
}

int list_sort_check(List* lp) {
	if (lp == NULL || lp->item == NULL || lp->next == NULL ||
			lp->next->item == NULL) {

		if (debug) printf("list_sort_check failed sanity "
				"check.\n");
		return FALSE;
	}

	return TRUE;
}

int list_destroy(List* lp) {
	/* variable to hold the length/size of the list in terms of
	 * number of elements in the chain. */
	int size;

	/* counter for the loop */
	int cnt;

	/* max number of elements - gets decremented with each iteration
	 * of loop. */
	int max;

	/* tracker */
	List* trk;

	/* sanity check */
	if (lp == NULL || lp->next == NULL) {
		if (debug) printf("list_destroy failed sanity check. "
			"List provided was empty, or no next item.\n");
		return FALSE;
	}
	
	/* --- */

	size = 0;
	trk = lp;

	/* figure out size of List */
	while (trk != NULL) {
		++size;
		trk = trk->next;
	}

	if (debug) printf("Size: %d\n", size);

	/* ``start from the outside and work your way in!''
	 * 	-- Molly Brown	*/

	trk = lp;
	max = size;

	/* it took a while for me to get the math right here, but
	 * essentially, we want to free all ->items and all Lists
	 * *except* the head (lp). So max gets set to the full size, but
	 * we have to subtract a 1 and a 2 here and there to count from
	 * 0. */

	while (max > 1) {

		for (cnt = 0; cnt < max-2; ++cnt) {
			trk = trk->next;
		}

		if (trk->next->item)
			free(trk->next->item);
		free(trk->next);

		/* reset the tracker to the beginning and decrement max
		 * since we have deleted one node. */

		trk = lp;	
		--max;
	}

	if (lp->item) {
		if (debug) printf("freeing lp->item: %p\n", (void*)lp->item);
		free(lp->item);
	}
	if (debug) printf("freeing lp: %p\n", (void*)lp);
	free(lp);

	return TRUE;
}

/* common macro for next 2 functions */
#define ERR_IF_HEAD_EXISTS \
	if (lp->item != NULL) { \
		fprintf(stderr, \
			"Tried to add head where already exists.\n"); \
		return FALSE; \
	}

/* add head of any pointer type to List. You must allocate memory
 * for the node first. */

int list_add_head(List* lp, void* node) {

	if (node == NULL) {
		if (debug) fprintf(stderr,
			"Node provided points to no data.\n");
		return FALSE;
	}

	ERR_IF_HEAD_EXISTS
	
	lp->item = node;
	lp->next = NULL;

	return TRUE;
}

/* convenience function to make a string the head of the List.
 * Memory allocation is baked into the function. */

int list_add_head_str(List* lp, const char* str) {
	char* sp;

       	sp = malloc(strlen(str) + 1);
	strcpy(sp, str);

	if (sp == NULL) {
		fprintf(stderr,
			"Invalid null string provided.\n");
		return FALSE;
	}

	ERR_IF_HEAD_EXISTS

	lp->item = sp;
	lp->next = NULL;

	return TRUE;
}

#undef ERR_IF_HEAD_EXISTS

/* common macro for next 2 functions */
#define ERR_IF_NO_HEAD \
	if (lp->item == NULL) { \
		fprintf(stderr, \
			"Tried to add string where no head exists. " \
			"You forgot to call list_add_head(_string). " \
			"Aborting.\n"); \
		exit(1); \
	}

List* list_add_node(List* lp, void* node) {
	List* newlp;
	List* trk;

	ERR_IF_NO_HEAD

	trk = lp;
	newlp = list_init();

	if (newlp == NULL) return NULL;

	newlp->item = node;

	while (trk != NULL) {
		if (trk->next == NULL) {
			trk->next = newlp;
			break; 
		}
		trk = trk->next;
	}

	return newlp;
}

List* list_add_string(List* lp, const char* str) {
	List* newlp;
	List* trk;
	char* sp;

	ERR_IF_NO_HEAD

	newlp = list_init();

	if (newlp == NULL) return NULL;

       	sp = malloc(strlen(str) + 1);
	strcpy(sp, str);

	newlp->item = sp;

	trk = lp;

	while (trk != NULL) {
		if (trk->next == NULL) {
			trk->next = newlp;
			break; 
		}
		trk = trk->next;
	}

	return newlp;
}

int list_item_swap(List* a, List* b) {
	void* tmp;

	if (a == NULL || b == NULL) return FALSE;

	tmp = a->item;
	a->item = b->item;
	b->item = tmp;

	return TRUE;
}

int list_sort_str_abc(List* lp) {
	/* tracker */
	List* trk;

	/* boolean to see if a swap happened - if so, we need to run the
	 * bubble sort loop again and again until no more swaps are
	 * necessary. */
	int swp;

	/* sanity checking */
	if ( ! list_sort_check(lp) ) return FALSE;

	/* I believe this is called bubble sort. I finally caved in and
	 * looked up generally how to do this, but implemented it myself.
	 * */
	swp = 1;

	while (swp == 1) {
		swp = 0;
		trk = lp;

		while (trk != NULL) {
			if ( trk->next != NULL && trk->next->item != NULL
				&&
				strncmp(trk->item, trk->next->item, 5) > 0 )
			{
				list_item_swap(trk, trk->next);	
				swp = 1;
			}

			trk = trk->next;
		}
	}

	return TRUE;
}

List* list_search(List* lp, void* targ, int (*sfunc)(void*, void*)) {
	/* tracker */
	List* trk;
	
	/* sanity check */
	if (lp == NULL || targ == NULL || sfunc == NULL) {
		if (debug) fprintf(stderr,
			"list_search: NULL List, search target or "
			"search function provided. Aborting.\n");

		return NULL;
	}

	/* --- */

	trk = lp;

	while (trk != NULL) {
		if ( (*sfunc)(trk->item, targ) ) {
			printf("list_search: match found.\n");
			return trk;
		}
		trk = trk->next;
	}

	/* if all else fails... */
	return NULL;
}

int list_search_str(void* item, void* void_str) {
	char* srch_str;
	char* poss_str;

	/* sanity check */
	if (item == NULL || void_str == NULL) {
		if (debug) printf("list_search_str: "
				"NULL item or search string "
				"provided. Aborting.\n");
		return FALSE;
	}

	/* --- */

	srch_str = (char*)void_str;
	poss_str = (char*)item;
	
	if (debug >= 2) printf("list_search_str: comparing node: %s to "
			"search: %s\n", poss_str, srch_str);

	/* final analysis */

	if ( strcmp(srch_str, poss_str) == 0 ) {
		/* match */
		if (debug) printf("list_search_str: match.\n");
		return TRUE;
	} else {
		return FALSE;
	}
}

int list_delete_node(List* lp, List* node) {
	List* trk;
	List* tmp = NULL;

	/* sanity check */
	if (!lp || !node) {
		if (debug) printf("list_delete_node: "
			"List or Node provided is null. "
			"Aborting.\n");
		return FALSE;
	}

	if (!lp->next) {
		if (debug) printf("list_delete_node: "
			"Single-noded list provided. "
			"Aborting.\n");
		return FALSE;
	}

	/* --- */

	trk = lp;

	while (trk) {
		if (trk->next == node) {
			if (debug) printf("list_delete_node: "
				"Found match at addr: %p\n",
				(void*)trk->next);

			tmp = trk->next->next;
			trk->next = tmp;
			return TRUE;
		}
		trk = trk->next;
	}

	/* if all else fails */
	return FALSE;
}
